# color_mixer
### developer: [Ilja Lichtenberg](mailto:ilja.an@mail.de)
## _Application to create a distribution of colors by customers' preferences_

This application receives an input text file by command line arguments and evaluates which distribution of colors should be created. It considers the satisfaction of customer and seller.

## Build and deployment

- The application is started by default on port 3000.
- Please make sure this port is free on your system.
- Please also make sure to have npm installed.


### To test the app

```sh
1 cd [project_dir]
2 npm install
3 npm run test:unit
```

### To run the app

```sh
1 cd [project_dir]
2 npm install
3 npm run build
```

#### If you want to set the port:
```sh
3  PORT=8443 npm run build
```
#### If you want to set the input file:
```sh
3  npm run build -- --file=input_file_1.txt
3  npm run build -- --file=input_file_2.txt
3  npm run build -- --file=input_file_3.txt
3  npm run build -- --file=input_file_4.txt
```
#### If you submit your own input file you need to follow this rules: 
```
5
1 M 3 G 5 G
2 G 3 M 4 G
5 M
```
where:
```
First line: Available colors amount
Subsequent lines: Customer preferences (color matte/gloss)
```

## Components

### index.ts 
entry point of the application

### App.ts
routing and initialization of command_line_file_reader 

### model
contains all model classes that are used in this application

### picker
business logic part to select the colors based on customers' preferences

### routes
implementation for routes

### command_line_file_reader
reads the input text file and prints the resulting color distribution to command line 
