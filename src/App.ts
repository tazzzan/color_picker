import Router from "./routes/Router";
import CommandLineFileReader from "./command_line_file_reader/CommandLineFileReader";

class App {
    public router;

    constructor() {
        this.router = Router.DEFAULT;

        CommandLineFileReader.processAttached();
    }

}

export default new App().router.express;




