import Color from "../model/Color";
import PickerFormData from "../model/PickerFormData";
import Customer from "../model/Customer";
import ColorMix from "../model/ColorMix";

/*
Color picker creates a color mix that is based on the following rules:

- customer is satisfied already with only 1 color in 'matte'
- 1 color should be either in gloss or in mate
- customer has to get at least 1 color
- as few mattes should be picked as possible

 */
export default class ColorPicker {
    public data!: PickerFormData;

    constructor(data: PickerFormData) {
        this.data = data;
    }

    public createColorMix(): ColorMix {
        const colorMix = new ColorMix();

        this.data = this.pickColorsForCustomers();
        colorMix.addColors(this.data);

        return colorMix;
    }

    private pickColorsForCustomers(): PickerFormData {
        this.data.preferences.forEach((customer: Customer) =>
            customer.preferences.forEach((preference: Color) =>
                customer.shouldPickColor(this.isColorPreferencedByOther(preference, customer.id))
                    ? customer.pickedColor = preference
                    : {}
            ));

        return this.data;
    }

    private isColorPreferencedByOther(color: Color, customerId: String): boolean {
        return this.data.preferences
            .filter(customer => customer.id != customerId)
            .filter(customer => customer.preferences.filter(c => c.name == color.name).length > 0).length > 0;
    }

}
