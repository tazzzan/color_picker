import ColorPicker from "../ColorPicker";
import PickerFormData from "../../model/PickerFormData";
import NoSolutionExistsError from "../../model/exceptions/NoSolutionExistsError";
import TestInputs from "./TestInputs";

describe('should result in no solution error', () => {
    it('process input_text_2', async () => {
        const colorPicker = new ColorPicker(new PickerFormData(1, TestInputs.preferences_test_input_2()));
        const response = () => colorPicker.createColorMix();

        expect(response).toThrowError(NoSolutionExistsError);
    })
})

describe('should contain correct color distribution', () => {
    it('process input_text_1', async () => {
        const colorPicker = new ColorPicker(new PickerFormData(5, TestInputs.preferences_test_input_1()));

        const response = await colorPicker.createColorMix();
        const expectedDistribution = TestInputs.expected_distribution_test_input_1();

        expect(response).toHaveProperty('colors');
        expect(response).toEqual({'colors': expectedDistribution});
        expect(response.getColorDistribution()).toEqual('G G G G M');
    })
    it('process input_text_3', async () => {
        const colorPicker = new ColorPicker(new PickerFormData(5, TestInputs.preferences_test_input_3()));

        const response = await colorPicker.createColorMix();
        const expectedDistribution = TestInputs.expected_distribution_test_input_3();

        expect(response).toHaveProperty('colors');
        expect(response).toEqual({'colors': expectedDistribution});
        expect(response.getColorDistribution()).toEqual('G M G M G');
    })
    it('process input_text_4', async () => {
        const colorPicker = new ColorPicker(new PickerFormData(2, TestInputs.preferences_test_input_4()));

        const response = await colorPicker.createColorMix();
        const expectedDistribution = TestInputs.expected_distribution_test_input_4();

        expect(response).toHaveProperty('colors');
        expect(response).toEqual({'colors': expectedDistribution});
        expect(response.getColorDistribution()).toEqual('M M');
    })
})

