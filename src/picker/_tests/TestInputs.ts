import Color, {ColorName} from "../../model/Color";
import Customer from "../../model/Customer";

const uuid = require("uuid");

export default class TestInputs{

    static expected_distribution_test_input_1() {
        return [
            new Color(false, ColorName.BLUE),
            new Color(false, ColorName.RED),
            new Color(false, ColorName.YELLOW),
            new Color(false, ColorName.BROWN),
            new Color(true, ColorName.PURPLE)
        ];
    }

    static preferences_test_input_1() {
        return [...new Array(3)]
            .map((_) => new Customer(uuid.v4()))
            .map((customer, index) => {
                let preferences: Color[] = [];

                switch (index) {
                    case 0:
                        preferences = [
                            new Color(true, ColorName.BLUE),
                            new Color(false, ColorName.YELLOW),
                            new Color(false, ColorName.PURPLE)
                        ]
                        break;
                    case 1:
                        preferences = [
                            new Color(false, ColorName.RED),
                            new Color(true, ColorName.YELLOW),
                            new Color(false, ColorName.BROWN)
                        ]
                        break;
                    case 2:
                        preferences = [
                            new Color(true, ColorName.PURPLE)
                        ]
                        break;
                }

                customer.preferences = preferences;
                return customer;
            });
    }

    static preferences_test_input_2() {
        return [...new Array(2)]
            .map((_) => new Customer(uuid.v4()))
            .map((customer, index) => {
                let preferences: Color[] = [];

                switch (index) {
                    case 0:
                        preferences = [
                            new Color(false, ColorName.BLUE),
                        ]
                        break;
                    case 1:
                        preferences = [
                            new Color(true, ColorName.BLUE),
                        ]
                        break;
                }

                customer.preferences = preferences;
                return customer;
            });
    }


    static expected_distribution_test_input_3() {
        return [
            new Color(false, ColorName.BLUE),
            new Color(true, ColorName.RED),
            new Color(false, ColorName.YELLOW),
            new Color(true, ColorName.BROWN),
            new Color(false, ColorName.PURPLE)
        ];
    }

    static preferences_test_input_3() {
        return [...new Array(14)]
            .map((_) => new Customer(uuid.v4()))
            .map((customer, index) => {
                let preferences: Color[] = [];

                switch (index) {
                    case 0:
                        preferences = [
                            new Color(true, ColorName.RED),
                        ]
                        break;
                    case 1:
                        preferences = [
                            new Color(false, ColorName.PURPLE),
                        ]
                        break;
                    case 2:
                        preferences = [
                            new Color(false, ColorName.PURPLE),
                            new Color(false, ColorName.BLUE),
                            new Color(true, ColorName.BROWN),
                        ]
                        break;
                    case 3:
                        preferences = [
                            new Color(false, ColorName.YELLOW)
                        ]
                        break;
                    case 4:
                        preferences = [
                            new Color(false, ColorName.PURPLE)
                        ]
                        break;
                    case 5:
                        preferences = [
                            new Color(false, ColorName.YELLOW),
                            new Color(false, ColorName.PURPLE),
                            new Color(false, ColorName.BLUE),
                        ]
                        break;
                    case 6:
                        preferences = [
                            new Color(false, ColorName.YELLOW),
                        ]
                        break;
                    case 7:
                        preferences = [
                            new Color(true, ColorName.RED),
                        ]
                        break;
                    case 8:
                        preferences = [
                            new Color(false, ColorName.PURPLE),
                            new Color(false, ColorName.BLUE),
                        ]
                        break;
                    case 9:
                        preferences = [
                            new Color(true, ColorName.RED)
                        ]
                        break;
                    case 10:
                        preferences = [
                            new Color(false, ColorName.PURPLE)
                        ]
                        break;
                    case 11:
                        preferences = [
                            new Color(true, ColorName.BROWN)
                        ]
                        break;
                    case 12:
                        preferences = [
                            new Color(false, ColorName.PURPLE),
                            new Color(true, ColorName.BROWN),
                        ]
                        break;
                }

                customer.preferences = preferences;
                return customer;
            });
    }


    static expected_distribution_test_input_4() {
        return [
            new Color(true, ColorName.BLUE),
            new Color(true, ColorName.RED),
        ];
    }

    static preferences_test_input_4() {
        return [...new Array(2)]
            .map((_) => new Customer(uuid.v4()))
            .map((customer, index) => {
                let preferences: Color[] = [];

                switch (index) {
                    case 0:
                        preferences = [
                            new Color(false, ColorName.BLUE),
                            new Color(true, ColorName.RED),
                        ]
                        break;
                    case 1:
                        preferences = [
                            new Color(true, ColorName.BLUE),
                        ]
                        break;
                }

                customer.preferences = preferences;
                return customer;
            });
    }
}