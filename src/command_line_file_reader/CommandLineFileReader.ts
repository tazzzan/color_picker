import PickerFormData from "../model/PickerFormData";
import ColorPicker from "../picker/ColorPicker";
import NoSolutionExistsError from "../model/exceptions/NoSolutionExistsError";

const minimist = require('minimist');

/*
Reads the file from command line during the start up of the application.

- Firstly, converts the text file to 'PickerFormData'
- Secondly, creates a color mix for customers preferences
- Thirdly, prints the distribution to the command line

When error occurs the error is printed to the command line.
When no color mix is possible corresponding message
 */
export default class CommandLineFileReader {

    public static async processAttached() {
        const file = minimist(process.argv.slice(2))['file'];

        file ?
            await PickerFormData
                .fromFile(file)
                .then(formData => new ColorPicker(formData).createColorMix())
                .catch(error => CommandLineFileReader.handleNoSolution(error))
                .then(colorMix => colorMix ? colorMix.getColorDistribution() : {})
                .then(distribution => console.log(distribution))
            : this.handleNoSolutionWithReason(NoSolutionExistsError.DEFAULT, 'Reason: no input provided');
    }

    private static handleNoSolutionWithReason(error: Error, reason: String) {
        this.handleNoSolution(error);
        console.error(reason);
    }

    private static handleNoSolution(error: Error) {
        if (error instanceof NoSolutionExistsError) {
            console.error(error.message);
        }
    }
}