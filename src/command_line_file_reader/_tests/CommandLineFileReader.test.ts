import cmdFileReader from '../CommandLineFileReader'
import TestInputs from "./TestInputs";

const consoleSpyLog = jest.spyOn(console, 'log').mockImplementation()
const consoleSpyError = jest.spyOn(console, 'error').mockImplementation()
const mockArgv = require('mock-argv')

describe('should result in no solution command line output', () => {
    it('process no input text', async () => {
        await cmdFileReader.processAttached();
        expect(consoleSpyError).toHaveBeenCalledWith('No solution exists');
        expect(consoleSpyError).toHaveBeenLastCalledWith('Reason: no input provided');
    })
    it('process input_text_2', async () => {
        mockArgv(TestInputs.mockCommandLineProperties('input_file_2.txt'));
        await cmdFileReader.processAttached();
        expect(consoleSpyError).toHaveBeenLastCalledWith('No solution exists');
    })
})

describe('should result in color distribution command line output', () => {
    it('process input_text_1', async () => {
        mockArgv(TestInputs.mockCommandLineProperties('input_file_1.txt'));
        await cmdFileReader.processAttached();
        expect(consoleSpyLog).toHaveBeenLastCalledWith(TestInputs.expected_distribution_test_input_1());
    })
    it('process input_text_3', async () => {
        mockArgv(TestInputs.mockCommandLineProperties('input_file_3.txt'));
        await cmdFileReader.processAttached();
        expect(consoleSpyLog).toHaveBeenLastCalledWith(TestInputs.expected_distribution_test_input_3());
    })
    it('process input_text_4', async () => {
        mockArgv(TestInputs.mockCommandLineProperties('input_file_4.txt'));
        await cmdFileReader.processAttached();
        expect(consoleSpyLog).toHaveBeenLastCalledWith(TestInputs.expected_distribution_test_input_4());
    })
})
