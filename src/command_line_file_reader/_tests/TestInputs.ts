
export default class TestInputs {

    static expected_distribution_test_input_1() {
        return 'G G G G M';
    }

    static expected_distribution_test_input_3() {
        return 'G M G M G';
    }

    static expected_distribution_test_input_4() {
        return 'M M';
    }

    static mockCommandLineProperties(filename: String) {
        return () =>  {
            process.argv = [
                'any',
                'any',
                '--file=' + filename,
            ]
        };
    }
}