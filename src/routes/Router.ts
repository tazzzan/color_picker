const express = require('express');
const port = process.env.PORT || 3000


export default  class Router{
    public static DEFAULT = new Router();

    public express;

    constructor() {
        this.express = express();
        this.mountRoutes();
    }

    private mountRoutes(): void {
        const router = express.Router();

        router.get('/', (req: any, res: any) => {
            res.json({
                message: 'Welcome to color picker! Your request date: ' + Date()
            });
        })

        this.express.use('/', router)

        this.express.listen(port, (err: any) => {
            if (err) {
                return console.log(err)
            }

            return console.log(`server is listening on ${port}`)
        })
    }

}