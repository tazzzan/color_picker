export default class NoSolutionExistsError extends Error {
    public static DEFAULT = new NoSolutionExistsError('No solution exists');

    constructor(m: string) {
        super(m);

        Object.setPrototypeOf(this, NoSolutionExistsError.prototype);
    }
}