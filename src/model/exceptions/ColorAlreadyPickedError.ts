export default class ColorAlreadyPickedError extends Error {
    constructor(m: string) {
        super(m);

        Object.setPrototypeOf(this, ColorAlreadyPickedError.prototype);
    }
}