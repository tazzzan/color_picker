import Color from "./Color";
import Customer from "./Customer";
import PickerFormData from "./PickerFormData";
import ColorAlreadyPickedError from "./exceptions/ColorAlreadyPickedError";
import NoSolutionExistsError from "./exceptions/NoSolutionExistsError";

export default class ColorMix {
    public colors: Color[] = [];

    public addColors(data: PickerFormData) {
        try {
            this.addPickedColors(data.preferences);
        } catch (ColorAlreadyPickedError) {
            throw NoSolutionExistsError.DEFAULT;
        }

        if (this.colors.length != data.colorsAmount) {
            this.addNotPickedColors(data);
        }
    }

    public getColorDistribution() {
        return this.colors.map(color => color.isMatte ? 'M' : 'G').join(' ');
    }

    private addPickedColors(preferences: Customer[]) {
        preferences.forEach((customer: Customer, index: number) => {
            try {
                this.addColor(customer);
            } catch (ColorAlreadyPickedError) {
                if (index == preferences.length - 1) {
                    throw new ColorAlreadyPickedError.DEFAULT;
                }
            }
        });
    }

    private addNotPickedColors(data: PickerFormData) {
        for (let i = 1; i < data.colorsAmount; i++) {
            if (this.colors.filter(color => Number(color.name) == i).length == 0) {
                this.colors.push(new Color(false, i));
                this.sort();
            }
        }
    }

    private addColor(customer: Customer) {
        const pickedColor = customer.pickedColor;

        if (pickedColor) {
            const alreadyPicked = this.colors.filter(color => color.name == pickedColor.name);

            if (alreadyPicked.length == 0) {
                this.colors.push(pickedColor);
                this.sort();
            } else {
                if (alreadyPicked.filter(color => color.isMatte == pickedColor.isMatte).length == 0) {
                    throw new ColorAlreadyPickedError(pickedColor.name + ' already picked!');
                }
            }
        }
    }

    private sort() {
        this.colors.sort((c1, c2) => Number(c1.name) - Number(c2.name));
    }
}
