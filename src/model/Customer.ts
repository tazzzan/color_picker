import Color, {ColorName} from "./Color";

const uuid = require("uuid");

export default class Customer {
    public id: String;
    public preferences: Color[] = [];
    public pickedColor!: Color;

    constructor(id: String) {
        this.id = id;
    }

    public static fromFormData(line: String): Customer {
        const customer = new Customer(uuid.v4());
        const array = line.split(' ');

        let matte = false;
        let color = ColorName.BLUE;

        array
            .map((value: any, index: number) => {
                isNaN(value)
                    ? matte = value === 'M'
                    : color = value;

                return index % 2 != 0 ? new Color(matte, color) : null;
            })
            .forEach((value: any) => value ? customer.preferences.push(value) : {});

        return customer;
    }

    public shouldPickColor(preferencedByOthers: boolean) {
        return !this.isSatisfiedByPicker()
            || !this.isSatisfiedByPreference()
            || !preferencedByOthers;
    }

    isSatisfiedByPicker(): boolean {
        return this.pickedColor != null ? !this.pickedColor.isMatte : false;
    }

    isSatisfiedByPreference(): boolean {
        return this.pickedColor != null;
    }

}