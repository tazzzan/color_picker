export default class Color {
    public isMatte: boolean = false;
    public name: ColorName;

    constructor(isMatte: boolean, name: ColorName) {
        this.isMatte = isMatte;
        this.name = name;
    }
}

export enum ColorName {
    BLACK, BLUE, RED, YELLOW, BROWN, PURPLE, WHITE, GREEN, LILA, ORANGE, PINK, GREY
}