import Customer from "./Customer";
import NoSolutionExistsError from "./exceptions/NoSolutionExistsError";

const fs = require('fs');
const readline = require('readline');

/*
     This form data is used by color picker to decide how to mix the colors.

     'fromFile' method allows to read the following input file structure:
     - First line: Available color amount
     - Subsequent lines: Customer preferences (color matte/gloss)

     Example:

     5
     1 M 3 G 5 G
     2 G 3 M 4 G
     5 M

     Explanation:

     Color 1: M
     Color 2: G
     Color 3: G & M
     Color 4: G
     Color 5: G & M

 */
export default class PickerFormData {
    public colorsAmount: number;
    public preferences: Customer[];

    constructor(colorsAmount: number, customerPreferences: Customer[]) {
        this.colorsAmount = colorsAmount;
        this.preferences = customerPreferences;
    }

    public static async fromFile(file: any): Promise<PickerFormData> {
        let colorsAmount: number = 0;
        let customerPreferences: Customer[] = [];

        if (fs.existsSync(file)) {
            const content = fs.readFileSync(file, 'utf8');
            const lines = content.split(/\r?\n/);

            lines.forEach((value: string, index: number) => {
                if (index == 0) {
                    colorsAmount = Number(value);
                } else {
                    customerPreferences.push(Customer.fromFormData(value));
                }
            })
        } else {
            throw NoSolutionExistsError.DEFAULT;
        }

        return new PickerFormData(colorsAmount, customerPreferences);
    }

}